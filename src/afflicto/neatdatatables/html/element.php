<?php

namespace afflicto\neatdatatables\html;

class element {

	protected $type = 'div';
	protected $hasEndTag = true;
	protected $content = array();
	protected $attributes = array();
	protected $classes = array();

	public function setType($type = 'div') {
		$this->type = $type;
	}

	public function getType() {
		return $this->type;
	}

	public function hasEndTag($bool = true) {
		$this->hasEndTag = $bool;
	}

	public function setContent($content) {
		if (is_array($content)) {
			$this->content = $content;
		}elseif (is_string($content)) {
			$this->content = array($content);
		}
	}

	public function getContent() {
		return $this->content;
	}

	public function __get($key) {
		if (isset($this->content[$key])) {
			return $this->content[$key];
		}

		return null;
	}

	public function __set($key, $val) {
		$this->content[$key] = $val;
	}

	public function __isset($key) {
		return isset($this->content[$key]);
	}

	public function __unset($key) {
		unset($this->content[$key]);
	}

	public function attr($key, $val = '__undefined__') {
		if ($val === '__undefined__') {
			return (isset($this->attributes[$key])) ? $this->attributes[$key] : null;
		}
		$this->attributes[$key] = $val;
	}

	public function addClass($class) {
		if (is_string($class)) {
			$class = explode(' ', $class);
		}
		foreach($class as $cls) {
			$this->classes[$cls] = $cls;
		}
	}

	public function removeClass($class) {
		if (is_string($class)) {
			$class = implode(' ', $class);
		}
		foreach($class as $cls) {
			unset($this->classes[$cls]);
		}
	}

	public function buildAttributes() {
		$str = '';
		
		# attributes
		if (count($this->attributes) > 0) {
			foreach($this->attributes as $k => $v) {
				$str .= ' ' .$k .'="' .str_replace('"', '', $v) .'"';
			}
		}

		if (count($this->classes) > 0) {
			$str .= ' class="';
			foreach($this->classes as $cls) {
				$str .= $cls .' ';
			}
			$str .= '"';
		}

		return $str;
	}

	public function renderContent() {
		$str = '';
		foreach($this->content as $v) {
			if (is_string($v)) {
				$str .= $v;
			}else if (is_object($v)) {
				if (method_exists($v, '__toString')) {
					$str .= $v;
				}else if ($v instanceof element) {
					$str .= $v->display();
				}
			}
		}
		return $str;
	}

	public function display() {
		$str = '<' .$this->type .$this->buildAttributes() .'>';

		if ($this->hasEndTag) {
			$str .= $this->renderContent() .'</' .$this->type .'>';
		}

		return $str;
	}

}