<?php
#----------------------------------
#	Example 7: Sortable Data
#	Here's how to sort the data
#----------------------------------
require_once 'includes/boot.php';

use afflicto\neatdatatables\datatable;

datatable::jQuery(true);

$columns = array(
	'id' => '#',
	'first_name' => 'First name',
	'last_name' => 'Last name',
	'email' => 'Email',
	'country' => 'Country',
	'ip_address' => 'IP Address',
);

$dt = new datatable('users', $columns);
$dt->option('jumpMenu', true);
$dt->option('url', 'http://dev/neatdatatables/examples/sortable.php?page={page}&sort={column}&dir={direction}');


#----------------------------------
#   Here's how to enable sorting
#	we call ->sort() on our table
#	And we pass it an array of values that CAN be sorted.
#	And then the default value to sort by
#	the last argument is the URL with a {column} and {direction} (if you're using pagination, add {page} too!)
#----------------------------------

# Let's enable pagination too, remember that both URLS need a {sort}, {dir} and {page} parameters
$page = 1;
if (isset($_GET['page'])) {
	$page = $_GET['page'];
}
$dt->paginate(10, $page);


$sortBy = 'first_name';
if (isset($_GET['sort'])) {
	$sortBy = $_GET['sort'];
}
$dir = 'ASC';
if (isset($_GET['dir'])) {
	$dir = strtoupper($_GET['dir']);
}

# now for the sortable
$sortableColumns = array(
	'id','first_name', 'last_name','email'
);
$dt->sort($sortableColumns, $sortBy, $dir);


$table = $dt->display();
?><!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../src/css/theme.css">
	<!-- load jquery -->
	<script src="includes/jquery.min.js"></script>

	<!-- ...and the plugin -->
	<script src="../src/js/jQuery.neatDataTables.js"></script>
	<style>
		body {
			font-family: helvetica, sans-serif;
		}
	</style>

</head>
<body>
	<?=$table?>
</body>
</html>