<?php
#----------------------------------
#	Example 1: BASIC
#	
#	Here's the "hello world!" of NDT
#	We'll require boot.php for the purpose of the example,
#	then create a datatable instance, tell it to grab data from the 'users' table, then echo it out!
#----------------------------------


# I have created a simple bootstrap file that I will include in all the examples.
# The file simply makes sure there is a table named 'users' in our database.
# Please open it in examples/includes/boot.php and review the code, add your database connection info and so on.
require_once 'includes/boot.php';

# import the namespace
use afflicto\neatdatatables\datatable;


# Let's create a datatable, the constructor takes 3 arguments: the name of the database table and the columns we want.
# The columns are defined as an array where:
# The keys (id, first_name, ip_address etc) are the names for the columns while the values are human-readable names for our table headings!
# Let's create a table of users, with the columns we defined
$columns = array(
	'id' => '#',
	'first_name' => 'First name',
	'last_name' => null,
	'email' => 'Email',
	'country' => 'Country',
	'ip_address' => 'IP Address',
);
$dt = new datatable('users', $columns);

# You can also include other columns in the result, without displaying NDT displaying them as regular columns by setting their value to NULL.
# like this:
/*$columns = array(
	'last_name' => null,
);
Then, the last_name column will be included in the result, but not displayed.
You can then do this for example:
$dt->rewrite('first_name', function($user) {
	return $user['first_name'] .' ' .$user['last_name'];
});
*/


# You can also add where clauses like so
#$dt->where('first_name', 'LIKE', 'peter');
#$dt->where('age', '>', 21);

# To print our datatable out simply echo the result of the display method:
# (this method builds a string and does not echo, allowing you to save it for later etc)
# Let's just echo it right away!
echo $dt->display();