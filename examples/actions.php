<?php
#----------------------------------
#	Example 4: Actions
#	Sometimes, it is useful to add buttons to do actions for a specific record/row (a "user" in this case).
#	Like "delete", "edit" and so on, here's how:
#
#	Actions are displayed in a <ul> element and each action is in a <li> element.
#	You are free to add your own CSS to style NDT.
# 	However, I'll add the basic.css file for this example.
#----------------------------------
require_once 'includes/boot.php';

use afflicto\neatdatatables\datatable;


#create our DT (datatable)
$columns = array(
	'id' => '#',
	'first_name' => 'First name',
	'last_name' => 'Last name',
	'email' => 'Email',
	'country' => 'Country',
	'ip_address' => 'IP Address',
);

$dt = new datatable('users', $columns);



#add some rewrites
$dt->rewrite('email', function($user) {
	return '<a href="mailto://' .$user['email'] .'">' .$user['email'] .'</a>';
});


#----------------------------------
#    Ok, here's how to add actions, very similar syntax to rewriting
#----------------------------------
$dt->action(function($user) {
	return '<a href="http://example.com/users/edit/' .$user['id'] .'">Edit</a>';
});

$dt->action(function($user) {
	return '<a href="http://example.com/users/delete/' .$user['id'] .'">Delete</a>';
});


# we'll continue and use pagination as well
$page = 1;
if (isset($_GET['page'])) {
	$page = $_GET['page'];
	if ($page < 1) {
		$page = 1;
	}
}
$dt->paginate(8, $page, 'http://dev/neatdatatables/examples/actions.php?page={page}');


?><!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="../src/css/basic.css">
	<style>
		body {
			font-family: helvetica, sans-serif;
		}
	</style>
</head>
<body>
	<?php
	echo $dt->display();
	?>
</body>
</html>