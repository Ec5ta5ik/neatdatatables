<?php
#----------------------------------
#	Example 2: Pagination
#	
#	When we have a lot of data, we wan't to be careful loading too much at once.
#	It's a good idea to split up the data into "pages". Or, to "paginate" the data.
#	
#	Note: I have added a basic.css stylesheet, this is optional.
#----------------------------------
require_once 'includes/boot.php';

use afflicto\neatdatatables\datatable;

$columns = array(
	'id' => '#',
	'first_name' => 'First name',
	'last_name' => 'Last name',
	'email' => 'Email',
	'country' => 'Country',
	'ip_address' => 'IP Address',
);

$dt = new datatable('users', $columns);


#----------------------------------
#   Here's how we enable pagination on our table
#	We call the 'paginate' method and pass it two arguments:
#	The number of records/items to show on each "page" (10),
#	The current page (1)
#----------------------------------
$dt->paginate(8, 1);
# There we go, NDT will now limit each results set to maximum 8 records
# it will also give us the results corresponding to the page we say


# But, NDT will render pagination links for the user to navigate to other "pages".
# the "page" variable must be in the URL somewhere, if you're not using vanity urls or "clean urls" (using mod_rewrite under apache or the like)
# we can just use a GET parameter for that. Such that a request to http://mysite.com/users.php?page=1 will give us the first page and so on.

# here's some basic code
# the page variable defaults to 1, we say..
$page = 1;
if (isset($_GET['page'])) {
	$page = $_GET['page'];
	if ($page < 1) {
		$page = 1;
	}
}

# Lastly, give NDT a template URL to use for it's pagination links.
# It needs to have {page} somewhere it in, which NDT will replace with a page number.
$dt->option('url', 'http://dev/neatdatatables/examples/pagination.php?page={page}');

# on line 32, we called paginate(), because of the nature of this guided example file, we'll have to call it again!
# This time, passing it the $page variable from the URL!
# Now NDT knows what page to render, great!
$dt->paginate(8, $page);


?><!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="../src/css/basic.css">
</head>
<body>
	<?php echo $dt->display();?>
</body>
</html>