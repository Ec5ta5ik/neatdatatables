<?php
#----------------------------------
#	Example 5: THEME
#   If you do not have any styling for tables
#   You can use the premade 'theme.css'

#	Note: the theme.css INCLUDES all the styling found in the basic.css, so you don't need to add both, only one of them.
#----------------------------------
require_once 'includes/boot.php';

use afflicto\neatdatatables\datatable;

$columns = array(
	'id' => '#',
	'first_name' => 'First name',
	'last_name' => 'Last name',
	'email' => 'Email',
	'country' => 'Country',
	'ip_address' => 'IP Address',
);

$dt = new datatable('users', $columns);


#add some rewrites
$dt->rewrite('email', function($user) {
	return '<a href="mailto://' .$user['email'] .'">' .$user['email'] .'</a>';
});



#add some actions
$dt->action(function($user) {
	return '<a href="http://example.com/users/edit/' .$user['id'] .'">Edit</a>';
});

$dt->action(function($user) {
	return '<a href="http://example.com/users/delete/' .$user['id'] .'">Delete</a>';
});


$page = 1;
if (isset($_GET['page'])) {
	$page = $_GET['page'];
	if ($page < 1) {
		$page = 1;
	}
}
$dt->paginate(8, $page, 'http://dev/neatdatatables/examples/theme.php?page={page}');


?><!DOCTYPE html>
<html>
<head>
	
	<!-- Here we add the theme.css -->
	<link rel="stylesheet" href="../src/css/theme.css">
	<style>
		body {
			font-family: helvetica, sans-serif;
		}
	</style>

</head>
<body>

	<?php
	echo $dt->display();
	?>

</body>
</html>