<?php

# First, require the NDT Classes from the 'src' directory.
require_once '../src/afflicto/neatdatatables/html/element.php'; # <- this is just a simple html element class (the datatable class extends it)
require_once '../src/afflicto/neatdatatables/datatable.php';

# import the namespace to save us some extra typing
use afflicto\neatdatatables\datatable;


# NDT Uses PDO for it's database connection
# Change the "mysql" part to "postresql" or "nosql" etc.
# If you have problems with this, contact me on codecanyon, or consult php.net/manual/en/pdo.construct.php
$dsn = 'mysql:dbname=ndt;host=localhost';
$username = 'root';
$password = 'kake';

# Let's give NDT our connection info, such that it can grab data for us when we request it!
datatable::connect($dsn, $username, $password);